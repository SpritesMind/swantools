//
//	IDC File to disassemble WonderSwan (color) rom
//	by Kaneda
//
//	Useage:
//	  install wsc_loader.idc on your idc folder
//	  update ida.int/ida64.int
//	  open a .ws or .wsc file
//	  select WonderSwan loader
//
//  0.1 (17 Sept 2018 ): First loader version
//	0.2 (30 Oct  2019 ): All in one loader

#include <idc.idc>


extern isColor;
extern romSize;
extern sramSize;
extern eepromSize;

//-------------------------------------------------------------------------
static CW(off,name,cmt) {
  auto x;
  x = off;
  MakeWord(x);
  MakeName(x,name);
  MakeRptCmt(x,cmt);
}

//-------------------------------------------------------------------------
static CD(off,name,cmt) {
  auto x;
  x = off;
  MakeDword(x);
  MakeName(x,name);
  MakeRptCmt(x,cmt);
}

//-------------------------------------------------------------------------
static CB(off,name,cmt) {
  auto x;
  x = off;
  MakeByte(x);
  MakeName(x,name);
  MakeRptCmt(x,cmt);
}

static CS(off,end,name,cmt) {
  auto x;
  x = off;
  MakeStr(x, end);
  MakeName(x,name);
  MakeRptCmt(x,cmt);
}

static getEEPromSize(id)
{
	if (id == 0)	return 0;

	if (id == 0x10)	return 1;
	if (id == 0x20)	return 16;
	
	if (id > 0x10)
		Message(form("Unknow eeprom size %d", id) );
	
	return 0;
}

static getSramSize(id)
{
	if (id == 0)	return 0;
	if (id == 1)	return 64;
	if (id == 2)	return 256;
	if (id == 3)	return 1024;
	if (id == 4)	return 2*1024;
	if (id == 5)	return 4*1024;
	
	if (id < 0x10)
		Message(form("Unknow sram size %d", id) );
	
	return 0;
}

static getRomSize(id)
{
	if (id == 0)	return 0;
	
	if (id == 2)	return 4;
	if (id == 3) return 8;
	if (id == 4) return 16;
	//if (id == 5)	return 24;
	if (id == 6)	return 32;
	//if (id == 7)	return 48;
	if (id == 8) return 64;
	if (id == 9)	return 128;
	
	Message(form("Unknow rom size %d", id) );
	
	return 0;
}

static header( )
{
  auto addr, i;
  
  addr = MaxEA() - 11;

  //00h
  CB(addr+0,"","");
  
  //01h 
  CB(addr+1,"METADATA","publisher id");
 
  //02h
  i = Byte(addr+2);
  if (i == 0)
  {
	CB(addr+2,"","wonderswan");
	isColor = 0;
  }
  else
  {
	CB(addr+2,"","wonderswan color");
	isColor = 1;
  }

  //03h
  CB(addr+3,"","game id");
  
  //04h
  CB(addr+4,"","game revision");
 
  //05h
  i = Byte(addr+5);
  romSize = getRomSize(i);
  CB(addr+5,"",form("%dMbit rom", romSize));
 
  //06h
  i = Byte(addr+6);
  sramSize = getSramSize(i);
  eepromSize = getEEPromSize(i);
  if (i == 0)
	CB(addr+6,"","no save");
  else if (sramSize > 0)
	CB(addr+6,"",form("%dKBits SRAM", sramSize));
  else if (eepromSize > 0)
	CB(addr+6,"",form("%dKBits EEPROM", eepromSize));
 
  //07h
  i = Byte(addr+7);
  if (i & 1)
	CB(addr+7,"","flags - vertical");
  else
	CB(addr+7,"","flags - horizontal");
 
  //08h
  i = Byte(addr+8);
  if (i)
	CB(addr+8,"","use RTC");
  else
	CB(addr+8,"","no RTC");
  
  
  //09h
  CW(addr+9, "","CheckSum");
}

static wonderswan_setup()
{
	auto id, i;
	
	//MEMORY MAP
	SegCreate(0x00000,0x10000,0,0,0,2);
	SegRename(0x00000,"RAM");

	SegCreate(0x10000,0x20000,0x1000,0,0,2);
	SegRename(0x10000,"SRAM");
	
	SegCreate(0x20000,0x30000,0x2000,0,0,2);
	SegRename(0x20000,"ROM0");
	
	SegCreate(0x30000,0x40000,0x3000,0,0,2);
	SegRename(0x30000,"ROM1");
	
	for ( i=0; i< (0x100000-0x40000); i = i+0x10000 ) // += illegal on IDC
	{
		SegCreate(0x40000+i,0x40000+i+0x10000,(0x40000+i)/16,0,0,2);
		SegRename(0x40000+i,form("ROM2_page%.2d", i/0x10000));
	}

	//CONST
	id = AddEnum(-1,"WS_MEMORY_MAP",0x1100000);
	AddConstEx(id,"TILES_SEGMENT",	0x200,	-1);
	
	//TODO IVP
	//TODO CPU int names
	
	
}

static swan_init(size)
{
  auto addr, i, baseROM2;
   
  addr = size - 1;
  Message( form("Max Addr = 0x%05X\n", addr) );
  
  ExtLinA		(0,	0,	";");
  ExtLinA		(0,	1,	"; Default configuration");
  ExtLinA		(0,	2,	"; RAM   / page0 = 0x0 (ws ram)");
  
  //TOFIX
  ExtLinA		(0,	3,	"; SRAM0 / C1 = FF so SRAM's 0x0000 (FF << 16 & 0x1FFFF) (8Kbyte, see metadata)");
  
  i = (0xFF << 16);
  //i = i | 0x20000;
  i = i & addr;
  ExtLinA		(0,	4,	form("; ROM0  / C2 = FF so 0x%05X (rom) ((FF << 16 | 0x20000) & 0x%05X)", i, addr));
  
  i = (0xFF << 16);
  //i = i | 0x30000;
  i = i & addr; 
  ExtLinA		(0,	5,	form("; ROM1  / C3 = FF so 0x%05X (rom) ((FF << 16 | 0x30000) & 0x%05X)", i, addr));
  
  i = (0xFF << 20);
  i = i | 0x40000;
  i = i & addr; 
  baseROM2 = i;
  ExtLinA		(0,	6,	form("; ROM2  / C0 = FF so 0x%05X (rom) ((FF << 20 | 0x40000) & 0x%05X)", baseROM2, addr));
  
  i = baseROM2 + 0x10000;
  i = i & addr; 
  ExtLinA		(0,	7,	form("; page5 = 0x%05X + 0x10000 = 0x%05X (rom) ", baseROM2, i));
  
  i = baseROM2 + 0x20000;
  i = i & addr; 
  ExtLinA		(0,	8,	form("; page6 = 0x%05X + 0x20000 = 0x%05X (rom) ", baseROM2, i));

  i = baseROM2 + 0x30000;
  i = i & addr; 
  ExtLinA		(0,	9,	form("; page7 = 0x%05X + 0x30000 = 0x%05X (rom) ", baseROM2, i));

  i = baseROM2 + 0x40000;
  i = i & addr; 
  ExtLinA		(0,	10,	form("; page8 = 0x%05X + 0x40000 = 0x%05X (rom) ", baseROM2, i));

  i = baseROM2 + 0x50000;
  i = i & addr; 
  ExtLinA		(0,	11,	form("; page9 = 0x%05X + 0x50000 = 0x%05X (rom) ", baseROM2, i));

  i = baseROM2 + 0x60000;
  i = i & addr; 
  ExtLinA		(0,	12,	form("; pageA = 0x%05X + 0x60000 = 0x%05X (rom) ", baseROM2, i));

  i = baseROM2 + 0x70000;
  i = i & addr; 
  ExtLinA		(0,	13,	form("; pageB = 0x%05X + 0x70000 = 0x%05X (rom) ", baseROM2, i));

  i = baseROM2 + 0x80000;
  i = i & addr; 
  ExtLinA		(0,	14,	form("; pageC = 0x%05X + 0x80000 = 0x%05X (rom) ", baseROM2, i));

  i = baseROM2 + 0x90000;
  i = i & addr; 
  ExtLinA		(0,	15,	form("; pageD = 0x%05X + 0x90000 = 0x%05X (rom) ", baseROM2, i));
 
  i = baseROM2 + 0xA0000;
  i = i & addr; 
  ExtLinA		(0,	16,	form("; pageE = 0x%05X + 0xA0000 = 0x%05X (rom) ", baseROM2, i));
  
  i = baseROM2 + 0xB0000;
  i = i & addr; 
  ExtLinA		(0,	17,	form("; pageF = 0x%05X + 0xB0000 = 0x%05X (rom) ", baseROM2, i));
  
  
  header( );
 
  Message("Thanks for using WonderSwan loader\n");
}


//Verify the input file format
// li - loader_input_t object. See IDA help file for more information
// n - How many times we have been called
//Returns:
// 0 - file unrecognized
// Name of file type - if file is recognized
static accept_file(li, n) {
	auto jmp_mne;
	auto fileext;
	if (n)	return 0;
	
	fileext = GetInputFile();
	fileext = substr(fileext, strstr(fileext, ".")+1, -1);
	if ( (strstr(fileext, "ws") == 0) || (strstr(fileext, "wsc") == 0) )
	{
	
		li.seek(0x10, SEEK_END);
		jmp_mne = li.getc( );

		if (jmp_mne != 0xEA) {
		//	return 0;
		}
	
		li.seek(0x0, SEEK_SET);
		return "WonderSwan (Color) IDA Loader v0.2";
	}

	return 0;
}

// Load the file into the database
//      li      - loader_input_t object. it is positioned at the file start
//      neflags - combination of NEF_... bits describing how to load the file
//                probably NEF_MAN is the most interesting flag that can
//                be used to select manual loading
//      format  - description of the file format
// Returns: 1 - means success, 0 - failure
static load_file(li, neflags, format) {
	auto entrypoint, size, nbBytes;
	
	SetPrcsr("80286r");
	
	
	wonderswan_setup( );
  
	size = li.size();
	
	//load in ROM2
	entrypoint = (((0xFF << 20) | 0x40000) & (size-1));
	
	Message(form("Init at 0x%05X\n", entrypoint));
	nbBytes = 0x100000 - entrypoint;
	if (nbBytes > size)	nbBytes = size;
	

	loadfile(li, entrypoint, 0x40000, nbBytes);
	
	//TODO : loop to fill ROM2
	
	//load in ROM0
	loadfile(li, (((0xFF << 16) | 0x20000) & (size-1)), 0x20000, 0x10000);
	
	//load in ROM1
	loadfile(li, (((0xFF << 16) | 0x30000) & (size-1)), 0x30000, 0x10000);

	swan_init( size );
	
	Message(form("loaded at 0x%05X\n", entrypoint));
  
	MakeCode(0xFFFF0);
	MakeName(0xFFFF0,"startup");
	
	Wait();

	Jump(0xFFFF0);
  
	return 1;
}
