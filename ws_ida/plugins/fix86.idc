#include <idc.idc>


// This plug in aims to make easier 64K segmentation far call/jmp X86 analysis.
// Default IDA behaviors is to wait for a segment of the same base to exist on segment list
// for exemple, a jmp far ptr 0C001h:0001h won't be analysis if a segment C0010->XXXX doesn't exist.
// while a segment C0000 -> XXXXX is totally valid
// In this case, this plugin will interpret the jump to a jump to C000:0011, which is absolutely the same thing 
//
// What is supported for now
//

// 9A 		; far absolute direct call
// EA 		; far absolute direct jump
// FF /3 	; far absolute indirect call
// FF /5	; far absolute indirect jump (mod R/M supported : 2E)
//
// create segment64k if not exiting
//
//
// Please note, for some reason, auto jump doesn't work on IDA if A000h:0000h but works work 0xA000:0x0000 so I used it on some comments


class linear_address
{
	linear_address(segment, offset)
	{
		this.segment = segment;
		this.offset = offset;
	}

	get_address()
	{
		return (this.segment*16 + this.offset);
	}

	get_ws_segment()
	{
		return this.get_address()/16 & 0xF000;
	}

	get_ws_offset()
	{
		return (this.get_address() - this.get_ws_segment()*16);
	}
}


class fix86_plugin_t
{
    fix86_plugin_t( )
	{
		this.flags = 0;
		this.comment = "Fix some X86 instructions handling";
		this.help = "";
		this.wanted_name = "FiX86 for Wonderswan",
		this.wanted_hotkey  = "Alt-Ctrl-F";
	}
	
	init ()
	{
		//Message("%s : init\n", this.wanted_name);
		return PLUGIN_OK;
	}
	
	injectComment(ea, newComment)
	{
		auto oldComment = Comment(ea);
		if ( strstr(oldComment,"patched")>=0)	return;

		if (oldComment!="")	oldComment = sprintf("%s\n", oldComment);

		
		MakeComm(ea, sprintf("%spatched: %s", oldComment, newComment));

	}

	replaceFarPtr(addr, linear)
	{
		//OpAlt(addr, 0, sprintf("far ptr 0x%04X:%04X", linear.get_ws_segment(), linear.get_ws_offset()) );
		if ( Name(linear.get_address()) != "")
			OpAlt(addr, 0, sprintf("far ptr %s", Name(linear.get_address())) );					
		else
			OpAlt(addr, 0, sprintf("far ptr 0x%04X:%04X", linear.get_ws_segment(), linear.get_ws_offset()) );
	}

	createSegment(segment)
	{
		SegCreate(segment*16, segment*16 + 64*1024, segment, 0, saAbs, scPub);
	}
	
	run(arg)
	{
		auto addr, mnem, opcode, fileAddr;
		auto segment, offset;
		auto indirect, instr_op;
		auto mod, reg, rm;
		auto name;
		auto comment;
 		auto old_mnem;
		auto type;
		auto linear;

		addr = ScreenEA();
		fileAddr = addr - GetSegmentAttr(addr, SEGATTR_START);
		old_mnem = GetDisasm(addr);
		type = GetOpType(addr, 0);

		mnem = GetMnem(addr);
		opcode = Byte(addr);
		if (strstr(mnem, "jmp") == 0 )
		{
			if (opcode == 0xE9)
			{
				//Message("JMP near relative detected at 0x%05x %s (0x%05x)\n", addr, atoa(addr), fileAddr);
				//nothing
			}
			else if (opcode == 0xEA)
			{
				//get data
				segment = Word(addr+3);
				offset  = Word(addr+1);
				//Message("JMP far absolute detected at 0x%05x %s (0x%05x) ", addr, atoa(addr), fileAddr);
				//Message("to 0x%04x:%04x\n", segment, offset);

				//convert
				linear = linear_address(segment, offset);
				//Message("to 0x%04x:%04x\n", linear.get_ws_segment(), linear.get_ws_offset());
				//create segment
				this.createSegment( linear.get_ws_segment() );
				//make code
				MakeCode( linear.get_address() );


				if (segment != linear.get_ws_segment() )
				{
					//comment = sprintf("%s", old_mnem);
					//this.injectComment(addr, comment);
					this.replaceFarPtr(addr, linear);
				}


			}
			else if (opcode == 0xEB)
			{
				//Message("JMP short relative detected at 0x%05x %s (0x%05x)\n", addr, atoa(addr), fileAddr);
				//nothing
			}
			else if (opcode == 0xFF)
			{
				instr_op = Byte(addr+1);
				mod = instr_op >> 6;
				reg = (instr_op >>3) & 0x7;
				rm = instr_op & 0x7;
				//Message("MOD: 0x%x REG: 0x%x R/M: 0x%x\n", mod, reg, rm);
				if ( reg == 5 )
				{
					
					//Message("JMP far absolute indirect detected at 0x%05x %s (0x%05x) %d\n", addr, atoa(addr), fileAddr, type);
					
					if (instr_op == 0x2E) //type ==2
					{
						//get data
						segment = GetReg(addr, "ds");
						offset  = Word(addr+2);
						linear = linear_address(segment, offset);

						//Message("   defined at  0x%04x:%04x\n", segment, offset);
						//Message("   defined at  0x%04x:%04x\n", linear.get_ws_segment(), linear.get_ws_offset());
						name = sprintf("dword ptr [ds:%04X]", offset);
					}
					else
					{
						Message("Unsupported JMP far absolute indirect detected at 0x%05x => ", addr);
						Message("MOD: 0x%x REG: 0x%x R/M: 0x%x\n", mod, reg, rm);
						return;
					}

					OpAlt(addr, 0, name);

					//get indirect address
					indirect = segment*16 + offset;
					MakeWord(indirect);
					MakeWord(indirect+2);
					offset = Word(indirect);
					segment = Word(indirect+2);
					linear = linear_address(segment, offset);

					//make code
					this.createSegment( linear.get_ws_segment() );
					MakeCode( linear.get_address() );

					//comment indirect address
					//Message("   to 0x%04x:%04x\n", segment, offset);
					if (segment != linear.get_ws_segment() )
					{
						//Message("   to 0x%04x:%04x\n", linear.get_ws_segment(), linear.get_ws_offset());
						comment = sprintf("%s\n0x%04x:%04x -> 0x%04x:%04x", old_mnem, segment, offset, linear.get_ws_segment(), linear.get_ws_offset() );
						this.injectComment(addr, comment);
					}


				}
				else if (reg == 4)
				{
					//Message("JMP near absolute indirect detected at 0x%05x %s (0x%05x)\n", addr, atoa(addr), fileAddr);
				}
				
			}



		}
		else if (strstr(mnem, "call") == 0 )
		{
//			Message("CALL detected at 0x%05x %s (0x%05x)\n", addr, atoa(addr), fileAddr);
			if (opcode == 0x9A)
			{
				//get data
				segment = Word(addr+3);
				offset  = Word(addr+1);
				//convert
				linear = linear_address(segment, offset);

//				Message("CALL far absolute detected at 0x%05x %s (0x%05x) ", addr, atoa(addr), fileAddr);
//				Message("to 0x%04x:%04x\n", segment, offset);
//				Message("to 0x%04x:%04x\n", linear.get_ws_segment(), linear.get_ws_offset());

				//create segment
				this.createSegment( linear.get_ws_segment() );
				//make code
				MakeCode( linear.get_address() );

				if (segment != linear.get_ws_segment() )
				{
					//comment = sprintf("%s", old_mnem);
					//this.injectComment(addr, comment);
					this.replaceFarPtr(addr, linear);
				}

			}
			else if (opcode == 0xFF)
			{
				instr_op = Byte(addr+1);
				mod = instr_op >> 6;
				reg = (instr_op >>3) & 0x7;
				rm = instr_op & 0x7;
				//Message("MOD: 0x%x REG: 0x%x R/M: 0x%x\n", mod, reg, rm);
				if ( reg == 3 )
				{
					
					//Message("CALL far absolute indirect detected at 0x%05x %s (0x%05x) %d\n", addr, atoa(addr), fileAddr, type);
					
					if (instr_op == 0x2E) //type ==2
					{
						//get data
						segment = GetReg(addr, "ds");
						offset  = Word(addr+2);
						linear = linear_address(segment, offset);

						//Message("   defined at  0x%04x:%04x\n", segment, offset);
						//Message("   defined at  0x%04x:%04x\n", linear.get_ws_segment(), linear.get_ws_offset());
						name = sprintf("dword ptr [ds:%04X]", offset);
					}
					else
					{
						Message("Unsupported CALL far absolute indirect detected at 0x%05x => ", addr);
						Message("MOD: 0x%x REG: 0x%x R/M: 0x%x\n", mod, reg, rm);
						return;
					}


					OpAlt(addr, 0, name);

					//get indirect address
					indirect = segment*16 + offset;
					MakeWord(indirect);
					MakeWord(indirect+2);
					offset = Word(indirect);
					segment = Word(indirect+2);
					linear = linear_address(segment, offset);

					//make code
					this.createSegment( linear.get_ws_segment() );
					MakeCode( linear.get_address() );

					//comment indirect address
					//Message("   to 0x%04x:%04x\n", segment, offset);
					if (segment != linear.get_ws_segment() )
					{
						//Message("   to 0x%04x:%04x\n", linear.get_ws_segment(), linear.get_ws_offset());
						comment = sprintf("%s\n0x%04x:%04x -> 0x%04x:%04x", old_mnem, segment, offset, linear.get_ws_segment(), linear.get_ws_offset() );
						this.injectComment(addr, comment);
					}

				}
				else if (reg == 2)
				{
					//Message("CALL near absolute indirect detected at 0x%05x %s (0x%05x)\n", addr, atoa(addr), fileAddr);
				}
				
			}
		}
		else
		{
			//Message("nothing to fix at 0x%05x %s (0x%05x)\n", addr, atoa(addr), fileAddr);
		}
	}

	term()
	{
		//Message("%s : term\n", this.wanted_name);
	}	
}

static PLUGIN_ENTRY()
{
	return fix86_plugin_t();
}	
