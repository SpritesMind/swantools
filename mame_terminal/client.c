#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <sys/socket.h>
#include <resolv.h>
#include <arpa/inet.h>
#include <errno.h>

#define MY_PORT		345
#define MAX_BUF		2
#define MAX_CONNECTION 3  // Passed to listen()


int main(int argc, char *argv[])
{
	int portno = MY_PORT;
    int sockfd;
	struct sockaddr_in self;
	unsigned char buffer[MAX_BUF];
	
	if (argc ==2) {
		portno = atoi(argv[1]);
	}


	/** Create streaming socket */
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		perror("Socket");
		exit(errno);
	}

	/** Initialize address/port structure */
	bzero(&self, sizeof(self));
	self.sin_family = AF_INET;
	self.sin_port = htons(portno);
	self.sin_addr.s_addr = INADDR_ANY;

	/** Assign a port number to the socket */
    if (bind(sockfd, (struct sockaddr*)&self, sizeof(self)) != 0)
	{
		perror("socket:bind()");
		exit(errno);
	}

	/** Make it a "listening socket". Limit to MAX_CONNECTION connections */
	if (listen(sockfd, MAX_CONNECTION) != 0)
	{
		perror("socket:listen()");
		exit(errno);
	}
		int clientfd;
		struct sockaddr_in client_addr;
		//int addrlen=sizeof(client_addr);
		socklen_t addrlen =sizeof(client_addr);



		/** accept an incomming connection  */
		clientfd = accept(sockfd, (struct sockaddr*)&client_addr, &addrlen);
		printf("%s:%d connected\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));

	/** Server run continuously */
	while (1)
	{	

		/** Echo back the received data to the client */
		//send(clientfd, buffer, recv(clientfd, buffer, MAX_BUF, 0), 0);
		int result = recv(clientfd, buffer, MAX_BUF, 0);
		if(result == -1)
		{
			printf("error %d: %s\n", errno, strerror(errno));
			break;
		}
		else if(result == 0)
		{
			printf("Disconnected\n");
			break;
		}
		
		printf("Value received: %02x\n",buffer[0]);

	}
	/** Close data connection */
	close(clientfd);

	/** Clean up */
	close(sockfd);
	return 0;
}
