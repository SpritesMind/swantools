#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <argtable3.h>
#include "utils/file_utils.h"

#define PAGE_SIZE	(64*1024)

#define ERROR_BUFFER_SIZE 4 //max error logged

#define USE_STUB	( (rom0->ival[0] != 0xFF) || (rom1->ival[0] != 0xFF) )


struct arg_lit *help, *version;
struct arg_lit *color, *eeprom, *sram, *rtc, *vertical; 
struct arg_file *file, *output;
struct arg_int	*size_option;
struct arg_int	*ip, *segment, *rom0, *rom1;
struct arg_int	*publiserID, *gameID, *gameVersion;
struct arg_end *end;

/*
unsigned char rom_size_table[] = {
  0, // 1 Mbit - no game known 
  1, // 2 Mbit - no game known
  2, // 4 Mbit
  3, // 8 Mbit
  4, // 16 Mbit
  5, // 24 Mbit - no game known
  6, // 32 Mbit 
  7, // 48 Mbit  - no game known
  8, // 64 Mbit 
  9, // 128 Mbit
};
*/

void write8(void* data, unsigned char val)
{
  unsigned char* buf = data;
  *buf = val;
}
void write16(void* data, unsigned short val)
{
  unsigned char* buf = data;
  *buf = val & 0xFF;
  buf++;
  *buf = (val>>8) & 0xFF;
}


static void printver()
{
	printf("Binary to Wonderswan WS converter v0.3\n");
	printf("KanedaFr 2020\n");
	printf("based on trap0xF's wsheader concept\n");
}

static void doConvert()
{
	FILE *in, *out;
	unsigned char *rom;
	unsigned short int checksum;
	unsigned char sizeID = 0;
	unsigned char flags;
	unsigned char C0,C3,C2;
	
	unsigned long  size, newSize, pos;
	
	size = file_getSize((char *)file->filename[0]);

	in = fopen(( char *)file->filename[0], "rb");
	if (in == NULL)	return;
	

	newSize = (size + 16) - 1; // add enought room for header
	if ( USE_STUB )		newSize += 16;	//add enought room for stub


	if ( size_option->ival[0]== 1)
	{
			
		//commented size are not sure
		/*
		if (newSize <= 128*1024)
		{
			sizeID = 0;
			newSize = 128*1024;
		}
		else if (newSize <= 256*1024)
		{
			sizeID = 1;
			newSize = 256*1024;
		}
		else*/ if (newSize <= 512*1024)
		{
			sizeID = 2;
			newSize = 512*1024;
		}
		else if (newSize <= 1*1024*1024)
		{
			sizeID = 3;
			newSize = 1024*1024;
		}
		else if (newSize <= 2*1024*1024)
		{
			sizeID = 4;
			newSize = 2*1024*1024;
		}
		/*
		else if (newSize <= 3*1024*1024)
		{
			sizeID = 5;
			newSize = 3*1024*1024;
		}
		*/
		else if (newSize <= 4*1024*1024)
		{
			sizeID = 6;
			newSize = 4*1024*1024;
		}
		/*
		else if (newSize <= 6*1024*1024)
		{
			sizeID = 7;
			newSize = 6*1024*1024;
		}
		*/
		else if (newSize <= 8*1024*1024)
		{
			sizeID = 8;
			newSize = 8*1024*1024;
		}
		else //if (newSize <= 16*1024*1024)
		{
			sizeID = 9;
			newSize = 16*1024*1024;
		}
	}
	else if ( size_option->ival[0] == 0)
	{
		if (newSize % PAGE_SIZE)
			newSize = ((newSize/PAGE_SIZE)+1)*PAGE_SIZE;
			
		sizeID = 0xF;
	}
	
	rom = malloc(newSize);
	if (rom == NULL)
	{
		printf("error : can't allocate %ld bytes\n", newSize);
		fclose(in);
		return;
	}
	
	memset(rom, 0xFF, newSize);
	fread(rom, size, 1, in);
	fclose(in);
	
	flags = (vertical->count)?1:0;
	flags |= 4; //16bits bus
	
	
	C0 = C3 = C2 = 0xFF;
	
	if (segment->ival[0] == -1)
	{
		segment->ival[0] = 0x4000; //?4000 on rom side
		if (newSize == 512*1024)	segment->ival[0] = 0x8000; //0 on rom side
	}
  
	// jmp far seg:offset
	write8(rom + newSize-16+0, 0xEA);
	write16(rom + newSize-16+1, ip->ival[0]); 
	write16(rom + newSize-16+3, segment->ival[0]);

	write8(rom + newSize-16+5, 0);
	write8(rom + newSize-10+0, publiserID->ival[0]);
	write8(rom + newSize-10+1, (color->count)?1:0);
	write8(rom + newSize-10+2, gameID->ival[0]);
	write8(rom + newSize-10+3, gameVersion->ival[0]);
	write8(rom + newSize-10+4, sizeID);
	//TODO save size
	write8(rom + newSize-10+5, (sram->count)?1: ((eeprom->count)?0x10:0) ); //64Kbit SRAM or 1KBit EEPROM
	write8(rom + newSize-10+6, flags);
	write8(rom + newSize-10+7, (rtc->count)?1:0);


	if ( USE_STUB )	
	{
		C2 = rom0->ival[0] & 0xFF;
		C3 = rom1->ival[0] & 0xFF;
		
		//TODO : convert to near ?
		//jmp far FFF0:000E0 ->0xFFFE0
		//write8(rom + newSize-16+0, 0xEA);
		write16(rom + newSize-16+1, 0xE0); 
		write16(rom + newSize-16+3, 0xFFF0);
	
		//set ROM0
		write8(rom + newSize-32+0, 0xB8); //mov ax
		write16(rom + newSize-32+1, C2);		
		write8(rom + newSize-32+3, 0xE6); //out C2,al
		write8(rom + newSize-32+4, 0xC2);
		//set ROM1
		write8(rom + newSize-32+5, 0xB8); //mov ax
		write16(rom + newSize-32+6, C3);		
		write8(rom + newSize-32+8, 0xE6); //out C3,al
		write8(rom + newSize-32+9, 0xC3);
		
		//jmp far seg:offset 
		write8(rom + newSize-32+10, 0xEA);
		write16(rom + newSize-32+11, ip->ival[0]); 
		write16(rom + newSize-32+13, segment->ival[0]);
		
		
		write8(rom + newSize-32+15, 0x90); //nop (to get something clear on disasm)
	}
   
 




	checksum = 0;
	for(pos = 0; pos < newSize-2; pos++) {
		checksum += *(rom + pos);
	}
	write16(rom + newSize-10+8, checksum);

	out = fopen((char *)output->filename[0], "wb");
	if(out == NULL)
	{
		printf("error : can't create %s\n", output->filename[0]);
		free(rom);
		return;
	}
	fwrite(rom, newSize, 1, out);
	fclose(out);

	free(rom);
	printf("ROM size\n %ld bytes (0x%.06lX)\n", newSize, newSize);
	printf("Banks\n");
	printf(" ROM0: 0x%.2X\n",C2);
	printf(" ROM1: 0x%.2X\n",C3);
	printf(" ROM2: 0x%.2X\n",C0);
	printf("Mapping\n");
	printf(" WS side -> Game side\n");
	printf(" 0x20000 -> 0x%.5lX\n", ((C2 << 16) & (newSize-1)) );
	printf(" 0x30000 -> 0x%.5lX\n", ((C3 << 16) & (newSize-1)) );
	for (pos = 0x40000; pos <= 0xF0000; pos+=0x10000)
		printf(" 0x%.5lX -> 0x%.5lX\n", pos, ((C0 << 20) | pos) & (newSize-1));
	printf("Entry point\n 0x%.4X:0x%.4X\n", segment->ival[0], ip->ival[0]);
}



int main(int argc, char *argv[])
{
	int exitcode = 0;
	int err;

	void *argtable[] = {
        version = arg_litn(NULL, "version", 0, 1, "display version info"),
        help    = arg_litn(NULL, "help", 0, 1, "display this help"),
        file    = arg_file1("i", "input", "<infile>","input .exe file"),
        output  = arg_file0("o", "output", "<outfile>","output .ws file"),
		color	= arg_litn(NULL, "color", 0, 1, "WonderSwan Color game" ),
		size_option= arg_int0("s", "size", "<value>","auto resize (1:default) or only adjust to 64K boundary (0)"),
		ip   	= arg_int0(NULL, "ip", "<value>","start address (default xxxx:0000)"),
		segment = arg_int0(NULL, "segment", "<value>","start segment (default 0000:xxxx)"),
		rom0   	= arg_int0(NULL, "rom0", "<value>","add stub at FFFE0 to set ROM0 (default no stub / rom0 = FF)"),
		rom1   	= arg_int0(NULL, "rom1", "<value>","add stub at FFFE0 to set ROM1 (default no stub / rom1 = FF)"),
		publiserID   = arg_int0(NULL, "publisher", "<value>","ID between 0 and 0xFF (0x40+ recommanded)"),
        gameID  	 = arg_int0(NULL, "gameID","<value>","ID between 0 and 0xFF"),
        gameVersion  = arg_int0(NULL, "gameVersion","<value>","Version between 0 and 0xFF"),
		sram	= arg_litn(NULL, "sram", 0, 1, "include SRAM" ),
		eeprom	= arg_litn(NULL, "eeprom", 0, 1, "include EEPROM, not SRAM" ),
		rtc		= arg_litn(NULL, "rtc", 0, 1, "include RTC support" ),
		vertical= arg_litn(NULL, "vertical", 0, 1, "force vertical orientation" ),
  
        end     = arg_end(ERROR_BUFFER_SIZE),
    };
	if (arg_nullcheck(argtable) != 0)
	{
		printf("error: insufficient memory\n");
		exitcode = -1;
		goto exit;
    }
    
    //default values
    size_option->ival[0]=1;
    ip->ival[0]=0;
    segment->ival[0]=-1; //boot at 0000:0000

    rom0->ival[0]=0xFF;
    rom1->ival[0]=0xFF;
    
    publiserID->ival[0]=0xFF;
    gameID->ival[0]=0;
    gameVersion->ival[0]=0;

	//get values
    err = arg_parse(argc,argv,argtable);
	
    // special case 1: '--help' takes precedence over error reporting
    if (help->count > 0)
    {
		printver();
        printf("Usage:\tbin2ws");
        arg_print_syntax(stdout, argtable, "\n");
        arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		exitcode = 0;
		goto exit;
    }
	
	// special case 2
	if (version->count)
	{
		printver();
		exitcode = 0;
		goto exit;
	}
	
	// If the parser returned any errors then display them and exit
    if ((err > 0) || (argc == 1))
    {
        // print upto ERROR_BUFFER_SIZE error details contained in the arg_end struct
        arg_print_errors(stdout, end, "bin2ws");
        printf("Try '--help' for more information.\n");
		exitcode = -1;
		goto exit;
    }
    
    if ( (rom0->ival[0] > 0xFF) || (rom1->ival[0] > 0xFF) )
    {
        printf("Error : rom0/rom1 max value = 0xFF\n");
		exitcode = -1;
		goto exit;
    }
   
    
    //set output name if needed
    if (output->count == 0)	
	{
		output->filename[0] = calloc(FILENAME_MAX, 1);
		if (output->filename[0] == NULL)
		{
			printf("error : Not enough memory (%dbytes).\n",FILENAME_MAX);
			exitcode = -1;
			goto exit;
		}
		
		file_getNewFilename((char *)file->filename[0], (char *)output->filename[0], ".ws");
	}
	
	if (!file_isValid( (char *)file->filename[0]))
	{
		printf("error : binary file %s not found\n", file->filename[0]);
		exitcode = -1;
		goto exit;
	}
	 
    doConvert();
   
exit:
    // deallocate each non-null entry in argtable[]
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    return exitcode;
}
