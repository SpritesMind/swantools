#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "file_utils.h"


char file_isValid(char *file)
{
	FILE *handle;
	handle = fopen(file, "rb");
	if (handle == NULL) {
		//printf("File %s not found\n", file);
		return 0;
	}
	fclose(handle);

	return 1;
}


long file_getSize(char *file)
{
    long retValue;
	FILE *handle;

    if (!file_isValid(file))    return 0L;

	handle = fopen(file, "rb");
	fseek(handle, 0L, SEEK_END);
    retValue = ftell(handle);

    fseek(handle, 0L, SEEK_SET);
	fclose(handle);

	return retValue;
}

char *file_getExtension(char *filename) {
	char *dot = strrchr(filename, '.');
	if (!dot || dot == filename) return "";
	return (dot + 1);
}

char file_isFileType(char *filename, char *fileType)
{
    char *ext = file_getExtension(filename);
    return ( strncmp(ext, fileType, strlen(fileType)) == 0 );
}


void file_getNewFilename(char *source, char *dest, const char *ext)
{
	strcpy(dest, source);

	char *pFile = strrchr(dest, '/');
	pFile = ((pFile == NULL) ? dest : pFile + 1);
	// change extension
	char *pExt = strrchr(pFile, '.');
	if (pExt != NULL)
		strcpy(pExt, ext);
	else
		strcat(pFile, ext);
}


unsigned char *file_load(char *filename)
{
	FILE *in;
	unsigned char *buffer;
	long bufferSize = file_getSize(filename);
	
	if (bufferSize == 0L)	return NULL;
	
	in = fopen(filename,"rb");
	if (!in)	return NULL;
	
	buffer=calloc(bufferSize, 1);
	
	fread(buffer, bufferSize, 1, in);
	
	fclose(in);
	
	
	return buffer;
}
