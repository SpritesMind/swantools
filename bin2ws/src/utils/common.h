#ifndef __COMMON_H__
#define __COMMON_H__



#ifndef TRUE
#define TRUE	1
#define FALSE	0
#endif

#define ERROR_BUFFER_SIZE	20

//thanks StackOverflow ;)
#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })
	 
	 
typedef unsigned char bool;



#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif

#endif //__COMMON_H__
