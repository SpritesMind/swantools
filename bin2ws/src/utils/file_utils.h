#ifndef __FILE_UTILS_H__
#define __FILE_UTILS_H__

#ifdef __cplusplus
extern "C" {
#endif

char file_isValid(char *filename);
long file_getSize(char *file);
char *file_getExtension(char *filename);
char file_isFileType(char *filename, char *fileType);
void file_getNewFilename(char *source, char *dest, const char *ext);
unsigned char *file_load(char *filename);

#ifdef __cplusplus
}
#endif

#endif
